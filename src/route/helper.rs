use crate::auth::{ AuthResult, create_auth_context };
use warp::{ Filter, filters::BoxedFilter };

pub fn require_auth() -> BoxedFilter<(AuthResult,)> {
    warp::header::optional::<String>("authorization")
        .map(create_auth_context)
        .boxed()
}
