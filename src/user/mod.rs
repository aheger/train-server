pub mod models;

use bcrypt::{ DEFAULT_COST, hash, verify };
use diesel::prelude::*;

use crate::db::schema::users;
use crate::error::{ AppError, RequestError, ResponseError };
use crate::auth::models::Credentials;
use models::{ NewUser, User };

pub fn exists_by_id(id: i32) -> Result<(), AppError> {
    let connection = super::db::get_connection()?;

    let exists_expr = diesel::dsl::exists(users::table.filter(users::id.eq(id)));

    let user_exists = diesel::select(exists_expr)
        .get_result(&connection);

    match user_exists {
        Ok(true) => Ok(()),
        Ok(false) => Err(AppError::Response(ResponseError::NotFound)),
        Err(e) => {
            error!("{}", e);
            return Err(AppError::Internal);
        },
    }
}

pub fn get_by_id(id: i32) -> Result<User, AppError> {
    let connection = super::db::get_connection()?;

    let user_result = users::table
        .filter(users::id.eq(id))
        .first::<User>(&connection);

    match user_result {
        Ok(result) => Ok(result),
        Err(diesel::result::Error::NotFound) => Err(AppError::Response(ResponseError::NotFound)),
        Err(e) => {
            error!("{}", e);
            return Err(AppError::Internal);
        },
    }
}

pub fn get_by_credentials(credentials: &Credentials) -> Result<User, AppError> {
    let connection = super::db::get_connection()?;

    let user_result = users::table
        .filter(users::name.eq(&credentials.name))
        .first::<User>(&connection);

    if let Err(e) = user_result {
        match e {
            diesel::result::Error::NotFound => {
                return Err(AppError::Response(ResponseError::NotFound));
            },
            e => {
                error!("{}", e);
                return Err(AppError::Internal);
            },
        }
    }

    let user = user_result.unwrap();


    let verify_result = verify(&credentials.password, &user.password);

    match verify_result {
        Err(_) => Err(AppError::Internal),
        Ok(valid) => match valid {
            false => Err(AppError::Request(RequestError::AuthInvalid)),
            true => Ok(user),
        }
    }
}

pub fn delete_by_id(id: i32) -> Result<(), AppError> {
    let connection = super::db::get_connection()?;

    let result = diesel::delete(
            users::table.filter(users::id.eq(id))
        )
        .execute(&connection);

    match result {
        Ok(_) => Ok(()),
        Err(e) => {
            error!("{}", e);
            return Err(AppError::Internal);
        },
    }
}

pub fn create(mut new_user: NewUser) -> Result<User, AppError> {
    let connection = super::db::get_connection()?;

    if !validate_new(&new_user) {
        return Err(AppError::Request(RequestError::Invalid));
    }

    new_user.password = hash(&new_user.password, DEFAULT_COST).unwrap();

    // TODO remove this workaround and use returning() when switching to postgres
    let created_user_result = connection.transaction::<_, diesel::result::Error, _>(|| {
        diesel::insert_into(users::table)
            .values(&new_user)
            .execute(&connection)?;

        users::table
            .order(users::id.desc())
            .limit(1)
            .first::<User>(&connection)
    });

    match created_user_result {
        Ok(created_user) => Ok(created_user),
        Err(diesel::result::Error::DatabaseError(diesel::result::DatabaseErrorKind::UniqueViolation, _)) => {
            Err(AppError::Request(RequestError::Conflict))
        }
        Err(e) => {
            error!("{}", e);
            Err(AppError::Internal)
        },
    }
}

fn validate_new(new_user: &NewUser) -> bool {
    new_user.name.len() > 0
        && new_user.password.len() > 5
}
