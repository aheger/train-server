# train-server

Backend server for physical training applications.

## configuration

Create .env file
```
cp .env.example .env
```

`DATABASE_URL`

path to the database file

`RUST_LOG`

log level (see https://doc.rust-lang.org/1.1.0/log/index.html) 

`SERVER_PORT`

port of the server

`SERVER_SECRT`

secret that is used to verify authorization tokens.

***IMPORTANT: Change this to a secure secret***

## setup

Install the diesel CLI client
```
cargo install diesel_cli --features "sqlite"
```

Setup the database
```
diesel setup
```

Start the server
```
cargo run --release
```

## test configuration

Create .test.env file
```
cp .test.example.env .test.env
```

`TEST_HOST`  

URL to your test host  

## tests

Run the tests

```
cargo test
```
