#[cfg(test)]

mod config;
pub mod auth;
pub mod random;
pub mod user;

use config::{get_config, TestConfig};
use lazy_static::lazy_static;
use reqwest::blocking::{Client, Response};
use std::error::Error;

lazy_static! {
    static ref CONFIG: TestConfig = get_config();
    static ref CLIENT: Client = Client::new();
}

pub struct ResponseData {
    pub status: http::status::StatusCode,
    pub body: serde_json::Value,
}

fn build_url(route: &str) -> String {
    format!("{}{}", CONFIG.host, route)
}

fn parse_response(res: Response) -> Result<ResponseData, Box<dyn Error>> {
    Ok(ResponseData {
        status: res.status(),
        body: serde_json::from_reader(res)?
    })
}

#[allow(dead_code)]
pub fn get_json(route: &str) -> Result<ResponseData, Box<dyn Error>> {
    let res = CLIENT.get(&build_url(route))
        .send()?;

    parse_response(res)
}

#[allow(dead_code)]
pub fn get_json_with_token(route: &str, token: &str) -> Result<ResponseData, Box<dyn Error>> {
    let res = CLIENT.get(&build_url(route))
        .bearer_auth(token)
        .send()?;

    parse_response(res)
}

#[allow(dead_code)]
pub fn post_json(route: &str, data: serde_json::Value) -> Result<ResponseData, Box<dyn Error>> {
    let body = serde_json::to_vec(&data)
        .expect("invalid data for body");

    let res = CLIENT.post(&build_url(route))
        .body(body)
        .send()
        .expect(&format!("POST {} failed", route));

    parse_response(res)
}

