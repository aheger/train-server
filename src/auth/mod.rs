extern crate regex;

pub mod models;
mod token;

use regex::Regex;
use std::time::{SystemTime, UNIX_EPOCH};

use crate::error::{ AppError, RequestError };
use crate::user;
use models::{ Context, Credentials };
use token::{ verify_jwt, encode_jwt };

pub type AuthResult = Result<Context, AppError>;


// create authentication context from Authorization header
pub fn create_auth_context(auth_header_option: Option<String>) -> Result<Context, AppError> {
    let token = get_token_from_header(auth_header_option)?;

    let context_result = verify_jwt(&token);

    if context_result.is_err() {
        return Err(AppError::Request(RequestError::AuthInvalid));
    }

    let context = context_result.unwrap();

    match user::exists_by_id(context.user_id) {
        Ok(_) => Ok(context),
        Err(AppError::Internal) => Err(AppError::Internal),
        Err(_) => Err(AppError::Request(RequestError::AuthInvalid)),
    }
}

pub fn create_token(credentials: &Credentials) -> Result<String, AppError> {
    let user_result = user::get_by_credentials(&credentials);

    if user_result.is_err() {
        // do not propagate not found error to not give attackers any hint on
        // whether a user exists
        return Err(AppError::Request(RequestError::AuthInvalid));
    }

    let user = user_result.unwrap();

    let context = Context {
        user_id: user.id,
        exp: get_expiration_date()
    };

    match encode_jwt(&context) {
        Ok(token) => Ok(token),
        Err(_) => Err(AppError::Internal),
    }
}

static EXP_DAYS: i32 = 30;

fn get_expiration_date() -> i32 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Server time is before UNIX_EPOCH.")
        .as_secs() as i32
        + EXP_DAYS * 24 * 60 * 60

}

fn get_token_from_header(auth_header: Option<String>) -> Result<String, AppError> {
    if auth_header.is_none() {
        return Err(AppError::Request(RequestError::AuthRequired));
    }

    extract_bearer(&auth_header.unwrap())
        .ok_or(AppError::Request(RequestError::AuthInvalid))
}

lazy_static! {
    static ref BEARER_REGEX: Regex = Regex::new(
        r"Bearer ([^\s]+)"
    ).unwrap();
}

fn extract_bearer(auth_header: &String) -> Option<String> {
    let captures = BEARER_REGEX.captures(auth_header)?;

    Some(captures.get(1)?.as_str().to_owned())
}
