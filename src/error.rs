use std::error::Error as StdError;
use std::fmt::{self, Display};

#[derive(Debug)]
pub enum AppError {
    Internal,
    Request(RequestError),
    Response(ResponseError),
}
impl StdError for AppError {}

impl Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AppError::Internal => write!(f, "AppError::Internal"),
            AppError::Request(e) => write!(f, "AppError::Request({})", e),
            AppError::Response(e) => write!(f, "AppError::Response({})", e),
        }
    }
}

#[derive(Debug)]
pub enum RequestError {
    AuthInvalid,
    AuthRequired,
    Conflict,
    Invalid,
}
impl StdError for RequestError {}

impl Display for RequestError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match self {
            RequestError::AuthInvalid => "RequestError::AuthInvalid",
            RequestError::AuthRequired => "RequestError::AuthRequired",
            RequestError::Conflict => "RequestError::Conflict",
            RequestError::Invalid => "RequestError::Invalid",
        })
    }
}

#[derive(Debug)]
pub enum ResponseError {
    NotFound,
}
impl StdError for ResponseError {}

impl Display for ResponseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match self {
            ResponseError::NotFound => "ResponseError::NotFound",
        })
    }
}

