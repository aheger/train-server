use serde::{ Deserialize, Serialize };

#[derive(Clone)]
#[derive(Debug)]
#[derive(Deserialize)]
#[derive(Serialize)]
pub struct Context {
    pub user_id: i32,
    pub exp: i32,
}

#[derive(Debug)]
#[derive(Deserialize)]
pub struct Credentials {
    pub name: String,
    pub password: String,
}
