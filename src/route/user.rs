use crate::auth::AuthResult;
use crate::route::helper::{ require_auth };
use warp::{ Filter, filters::BoxedFilter, Reply, Rejection };
use crate::user;

pub fn index() -> BoxedFilter<(impl Reply, )> {
    warp::path("user")
        .and(
            post_user()
                .or(me())
                .or(delete_user())
        )
        .boxed()
}

fn me() -> BoxedFilter<(impl Reply,)> {
    warp::get()
        .and(warp::path("me"))
        .and(warp::path::end())
        .and(require_auth())
        .and_then(reply_auth_user)
        .boxed()
}

fn delete_user() -> BoxedFilter<(impl Reply, )> {
    warp::delete()
        .and(warp::path::end())
        .and(require_auth())
        .and_then(reply_delete_user)
        .boxed()
}

fn post_user() -> BoxedFilter<(impl Reply, )> {
    warp::post()
        .and(warp::path::end())
        .and(warp::body::json())
        .and_then(reply_create_user)
        .boxed()
}

async fn reply_create_user(new_user: user::models::NewUser) -> Result<impl Reply, Rejection> {
    let created_user_result = user::create(new_user);

    if let Err(e) = created_user_result {
        return Err(warp::reject::custom(e));
    }

    let created_user = created_user_result.unwrap();

    Ok(warp::reply::json(&created_user))
}

async fn reply_auth_user(auth_result: AuthResult) -> Result<impl Reply, Rejection> {
    if let Err(e) = auth_result {
        return Err(warp::reject::custom(e));
    }

    let user_result = user::get_by_id(auth_result.unwrap().user_id);

    if let Err(e) = user_result {
        return Err(warp::reject::custom(e));
    }

    let user = user_result.unwrap();

    Ok(warp::reply::json(&user))
}


async fn reply_delete_user(auth_result: AuthResult) -> Result<impl Reply, Rejection> {
    if let Err(e) = auth_result {
        return Err(warp::reject::custom(e));
    }

    let user_id = auth_result.unwrap().user_id;

    let result = user::delete_by_id(user_id);

    match result {
        Ok(_) => Ok(warp::reply()),
        Err(e) => Err(warp::reject::custom(e))
    }
}
