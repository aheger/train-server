#[cfg(test)]
mod common;

use common::post_json;
use common::auth::get_auth;
use common::user::{ UserCredentials, get_user };

use lazy_static::lazy_static;

use serde_json::json;

lazy_static! {
    static ref CREDENTIALS: UserCredentials = get_user();
}

#[test]
fn auth_valid() {
    get_auth();
}

#[test]
fn auth_name_invalid() {
    let result = post_json("/auth", json!({
            "name": "some-other-name",
            "password": CREDENTIALS.password,
        }))
        .expect("request failed");

    assert_eq!(result.status, 403);
}

#[test]
fn auth_password_invalid() {
    let result = post_json("/auth", json!({
            "name": CREDENTIALS.name,
            "password": "some-other-password",
        }))
        .expect("request failed");

    assert_eq!(result.status, 403);
}

#[test]
fn auth_missing() {
    let result = post_json("/auth", json!({}))
        .expect("request failed");

    assert_eq!(result.status, 400);
}
