#[cfg(test)]

pub struct TestConfig {
   pub host: String,
}

pub fn get_config() -> TestConfig {
    dotenv::from_filename(".test.env").ok();

    TestConfig {
        host: dotenv::var("TEST_HOST").expect("TEST_HOST must be set"),
    }
}

