#[cfg(test)]

use serde::{ Serialize };
use serde_json::json;

use super::post_json;
use super::random::*;

#[derive(Serialize)]
pub struct UserCredentials {
    pub name: String,
    pub password: String,
}

#[allow(dead_code)]
pub fn get_user() -> UserCredentials {
    let credentials = UserCredentials {
        name: get_prefixed_string("qa", 10),
        password: get_string(6),
    };

    let user = json!(credentials);

    let result = post_json("/user", user)
        .expect("request failed");

    assert_eq!(result.status, 200);
    assert!(result.body.get("id").is_some());
    assert_eq!(result.body["name"], credentials.name);

    credentials
}


