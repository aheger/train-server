use serde::{ Serialize };
use warp::{ Filter, filters::BoxedFilter, Reply, Rejection };

use crate::auth;

pub fn index() -> BoxedFilter<(impl Reply, )> {
    warp::post()
        .and(warp::path("auth"))
        .and(warp::path::end())
        .and(warp::body::json())
        .and_then(reply_token)
        .boxed()
}

#[derive(Serialize)]
struct TokenReply {
    token: String,
}

async fn reply_token(credentials: auth::models::Credentials) -> Result<impl Reply, Rejection> {
    let token_result = auth::create_token(&credentials);

    if let Err(e) = token_result {
        return Err(warp::reject::custom(e));
    }

    Ok(warp::reply::json(&TokenReply {
        token: token_result.unwrap()
    }))
}
