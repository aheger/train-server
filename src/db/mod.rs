pub mod schema;

use diesel::prelude::*;
use diesel::r2d2::*;
use std::env;

use crate::error::AppError;

type SqlitePool = Pool<ConnectionManager<SqliteConnection>>;
type SqlitePoolConnection = PooledConnection<ConnectionManager<SqliteConnection>>;

lazy_static! {
    static ref POOL:SqlitePool = init_pool();
}

fn init_pool() -> SqlitePool {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");

    let conn = SqliteConnection::establish(&database_url)
        .expect("could not connect to database.");

    conn.execute("PRAGMA synchronous = NORMAL; PRAGMA journal_mode = WAL;")
        .expect("could not write database configuration.");

    let manager = ConnectionManager::<SqliteConnection>::new(database_url);

    Pool::builder()
        .max_size(1)
        .build(manager)
        .expect("failed to create pool.")
}

pub fn get_connection() -> Result<SqlitePoolConnection, AppError> {
    let conn_result = POOL.get();
    match conn_result {
        Ok(conn) => Ok(conn),
        Err(e) => {
            error!("{}", e);
            Err(AppError::Internal)
        },
    }
}
