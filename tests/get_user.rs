#[cfg(test)]
mod common;

use common::{ get_json, get_json_with_token };
use common::auth::get_auth;

use lazy_static::lazy_static;

lazy_static! {
    static ref TOKEN: String = get_auth();
}

#[test]
fn get_user_me_valid() {
    let result = get_json_with_token("/user/me", TOKEN.as_ref())
        .expect("request failed");

    assert_eq!(result.status, 200);
    assert!(result.body.get("id").is_some());
    assert!(result.body["name"].as_str().is_some());
}

#[test]
fn get_user_me_no_auth() {
    let result = get_json("/user/me")
        .expect("request failed");

    assert_eq!(result.status, 401);
}

#[test]
fn get_user_me_invalid_auth() {
    let result = get_json_with_token("/user/me", "invalid-token")
        .expect("request failed");

    assert_eq!(result.status, 403);
}
