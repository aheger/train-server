#[cfg(test)]

use super::post_json;
use super::user::get_user;

use serde_json::json;

#[allow(dead_code)]
pub fn get_auth() -> String {
    let credentials = get_user();

    let result = post_json("/auth", json!(credentials))
        .expect("request failed");

    assert_eq!(result.status, 200);
    assert!(result.body.get("token").is_some());

    // serde_json::to_string(&result.body).expect("foo")
    String::from(result.body["token"].as_str().unwrap())
}

