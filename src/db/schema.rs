table! {
    exercises (id) {
        id -> Integer,
        name -> Text,
    }
}

table! {
    program_days (id) {
        id -> Integer,
        program_id -> Integer,
        name -> Text,
        position -> Integer,
    }
}

table! {
    program_reps (id) {
        id -> Integer,
        exercise_id -> Integer,
        program_day_id -> Integer,
        position -> Integer,
        ratio -> Integer,
        repetitions -> Integer,
        result -> Integer,
    }
}

table! {
    program_template_days (id) {
        id -> Integer,
        program_template_id -> Integer,
        name -> Text,
        position -> Integer,
    }
}

table! {
    program_template_reps (id) {
        id -> Integer,
        exercise_id -> Integer,
        program_template_day_id -> Integer,
        position -> Integer,
        ratio -> Integer,
        repetitions -> Integer,
    }
}

table! {
    program_templates (id) {
        id -> Integer,
        name -> Text,
    }
}

table! {
    programs (id) {
        id -> Integer,
        user_id -> Integer,
        name -> Text,
    }
}

table! {
    train_weights (exercise_id, user_id) {
        exercise_id -> Integer,
        user_id -> Integer,
        weight -> Float,
    }
}

table! {
    users (id) {
        id -> Integer,
        name -> Text,
        password -> Text,
    }
}

joinable!(program_days -> programs (program_id));
joinable!(program_reps -> exercises (exercise_id));
joinable!(program_reps -> program_days (program_day_id));
joinable!(program_template_days -> program_templates (program_template_id));
joinable!(program_template_reps -> exercises (exercise_id));
joinable!(program_template_reps -> program_template_days (program_template_day_id));
joinable!(programs -> users (user_id));
joinable!(train_weights -> exercises (exercise_id));
joinable!(train_weights -> users (user_id));

allow_tables_to_appear_in_same_query!(
    exercises,
    program_days,
    program_reps,
    program_template_days,
    program_template_reps,
    program_templates,
    programs,
    train_weights,
    users,
);
