#[macro_use]
extern crate diesel;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;

pub mod auth;
pub mod db;
pub mod error;
pub mod route;
pub mod user;

use dotenv::dotenv;
use std::env;
use warp::Filter;

#[tokio::main]
async fn main() {
    dotenv().ok();
    env_logger::init();

    // TODO create and verify a configuration struct from env vars

    let server_port = env::var("SERVER_PORT")
        .expect("SERVER_PORT must be set")
        .parse::<u16>()
        .expect("SERVER_PORT must be numeric");

    let routes = route::healthcheck()
        .or(route::auth())
        .or(route::user())
        .recover(route::handle_error)
        .with(warp::log("request"));

    warp::serve(routes).run(([127, 0, 0, 1], server_port)).await;
}
