extern crate jsonwebtoken as jwt;

use jwt::{decode, DecodingKey, encode, EncodingKey, Header, Validation};
use std::env;

use crate::auth::models::Context;

lazy_static! {
    static ref SECRET:String = {
        env::var("SERVER_SECRET").expect("SERVER_SECRET must be set.")
    };
}

pub fn encode_jwt(context: &Context) -> Result<String, jwt::errors::Error> {
    match encode(&Header::default(), context, &EncodingKey::from_secret(SECRET.as_ref())) {
        Ok(t) => Ok(t),
        Err(err) => {
            error!("{}", err);
            Err(err)
        },
    }
}

pub fn verify_jwt(token: &String) -> Result<Context, jwt::errors::Error> {
    let validation = Validation::default();

    match decode::<Context>(token, &DecodingKey::from_secret(SECRET.as_ref()), &validation) {
        Ok(c) => Ok(c.claims),
        Err(err) => {
            error!("{}", err);
            Err(err)
        },
    }
}
