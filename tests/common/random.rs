#[cfg(test)]

use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;

#[allow(dead_code)]
pub fn get_string(length: usize) -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length)
        .collect()
}


#[allow(dead_code)]
pub fn get_prefixed_string(prefix: &str, length: usize) -> String {
    let rnd_part: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length - prefix.len() - 1)
        .collect();

    format!("{}-{}", prefix, rnd_part)
}
