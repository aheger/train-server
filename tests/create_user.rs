#[cfg(test)]

mod common;

use serde_json::json;

use common::post_json;
use common::random::*;

#[test]
fn create_valid() {
    common::user::get_user();
}

#[test]
fn create_password_short() {
    let user_name = get_prefixed_string("qa", 10);

    let user = json!({
        "name": user_name,
        "password": get_string(5),
    });

    let result = post_json("/user", user)
        .expect("request failed");

    assert_eq!(result.status, 400);
}

#[test]
fn create_password_missing() {
    let user_name = get_prefixed_string("qa", 10);

    let user = json!({
        "name": user_name,
    });

    let result = post_json("/user", user)
        .expect("request failed");

    assert_eq!(result.status, 400);
}


#[test]
fn create_name_missing() {
    let user = json!({
        "password": get_string(5),
    });

    let result = post_json("/user", user)
        .expect("request failed");

    assert_eq!(result.status, 400);
}
