#[cfg(test)]

mod common;

use serde_json::json;
use assert_json_diff::assert_json_eq;

use common::get_json;

#[test]
fn health_check() {
    let result = get_json("/healthcheck").expect("request failed");

    assert!(result.status.is_success());

    assert_json_eq!(result.body, json!({
        "status": "OK"
    }));
}
