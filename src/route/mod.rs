use crate::error::{ AppError, RequestError, ResponseError };
use serde::Serialize;
use std::convert::Infallible;
use warp::{ Filter, filters::BoxedFilter, http::StatusCode, Reply, Rejection, reject };

impl reject::Reject for AppError {}

mod auth;
pub mod helper;
mod user;

pub fn auth() -> BoxedFilter<(impl Reply,)> {
    auth::index()
}

pub fn user() -> BoxedFilter<(impl Reply,)> {
    user::index()
}

#[derive(Debug)]
#[derive(Serialize)]
struct HealthcheckMessage {
    status: String,
}

pub fn healthcheck() -> BoxedFilter<(impl Reply,)> {
    warp::get()
        .and(warp::path("healthcheck"))
        .and(warp::path::end())
        .map(|| warp::reply::json(&HealthcheckMessage {
            status: String::from("OK")
        }))
        .boxed()
}

#[derive(Debug)]
#[derive(Serialize)]
struct ErrorMessage {
    code: u16,
    message: String,
}

pub async fn handle_error(err: Rejection) -> Result<impl Reply, Infallible> {
    let code;
    let message;

    if let Some(matched_err) = err.find::<AppError>() {
        code = match matched_err {
            AppError::Request(RequestError::AuthInvalid) => StatusCode::FORBIDDEN,
            AppError::Request(RequestError::AuthRequired) => StatusCode::UNAUTHORIZED,
            AppError::Request(RequestError::Conflict) => StatusCode::CONFLICT,
            AppError::Request(RequestError::Invalid) => StatusCode::BAD_REQUEST,
            AppError::Response(ResponseError::NotFound) => StatusCode::NOT_FOUND,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        };

        message = format!("{}", matched_err);
    } else if let Some(matched_err) = err.find::<warp::filters::body::BodyDeserializeError>() {
        code = StatusCode::BAD_REQUEST;
        message = format!("{}", matched_err);
    } else {
        error!("unhandled rejection: {:?}", err);
        code = StatusCode::INTERNAL_SERVER_ERROR;
        message = String::from("UNHANDLED_REJECTION");
    }

    let json = warp::reply::json(&ErrorMessage {
        code: code.as_u16(),
        message: message.into(),
    });

    return Ok(warp::reply::with_status(json, code));
}
