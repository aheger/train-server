use serde::{ Deserialize, Serialize };
use crate::db::schema::users;

#[derive(Debug)]
#[derive(Queryable)]
#[derive(Serialize)]
pub struct User {
    pub id: i32,
    pub name: String,
    #[serde(skip_serializing)]
    pub password: String,
}

#[derive(Debug)]
#[derive(Deserialize)]
#[derive(Insertable)]
#[table_name="users"]
pub struct NewUser {
    pub name: String,
    pub password: String,
}
